<section>
	<h2><?php _e( 'Download your data', 'wc-personal-data' ); ?></h2>

	<p><?php _e( 'Download all data we have for your account.', 'wc-personal-data' ); ?></p>

	<button id="wcpd-download-data"><?php _e( 'Download', 'wc-personal-data' ); ?></button>

	<div id="wcpd-download-loading" aria-hidden="true">
		<div class="wcpd__loader"></div>
		<p class="wcpd__text"><?php _e( 'We are preparing your data, please wait...', 'wc-personal-data' ); ?></p>
	</div>

	<p id="download-info" aria-hidden="true"></p>
</section>

<section>
	<h2><?php _e( 'Delete your account', 'wc-personal-data' ); ?></h2>

	<p><?php _e( 'Delete your account and all its data.', 'wc-personal-data' ); ?></p>

	<div id="deletion-popin" class="wcpd__popin" aria-hidden="true">
		<div id="deletion-content" aria-hidden="false">
			<p><?php _e( 'Account deletion', 'wc-personal-data' ); ?></p>
			<p><?php _e( 'Do you really want to delete your account and all its data ?', 'wc-personal-data' ); ?></p>

			<div>
				<button id="deletion-cancel"><?php _e( 'Cancel', 'wc-personal-data' ); ?></button>
				<button id="deletion-confirm"><?php _e( 'Delete', 'wc-personal-data' ); ?></button>
			</div>

			<p id="deletion-info"></p>
		</div>

		<div id="deletion-success" aria-hidden="true">
			<p><?php _e( 'Your account has successfully been deleted. You will be redirected to our homepage.', 'wc-personal-data' ); ?></p>
		</div>
	</div>

	<button id="wcpd-delete-account"><?php _e( 'Delete', 'wc-personal-data' ); ?></button>
</section>