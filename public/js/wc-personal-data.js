'use strict';

/**
 * AJAX
 */
function wcpdPost(args, callback, isFormData) {
	var isFormData = isFormData || false;

	var request = new XMLHttpRequest();

	if (isFormData) {
		var params = new FormData(args['form']);

		if (args['action']) params.append('action', args['action']);
		if (args['nonce']) params.append('nonce', args['nonce']);
	} else {
		var params = '';

		for (var key in args) {
			params += key + '=' + args[key] + '&';
		}params = params.substring(0, params.length - 1);
	}

	request.onload = function () {
		if (request.status >= 200 && request.status < 400) {
			callback(JSON.parse(request.responseText));
		} else {
			callback({ success: false, data: { error: 'server' } });
		}
	};

	request.onerror = function () {
		callback({ success: false, data: { error: 'connection' } });
	};

	request.open('POST', wcpd.ajaxUrl, true);
	if (!isFormData) request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.send(params);
}

(function (window, document) {
	var downloadDataButton = document.getElementById('wcpd-download-data');
	var downloadLoading = document.getElementById('wcpd-download-loading');
	var downloadInfo = document.getElementById('download-info');

	function downloadResponse(response) {
		downloadLoading.setAttribute('aria-hidden', true);
		downloadDataButton.disabled = false;

		if (response.success) {
			downloadInfo.setAttribute('aria-hidden', true);
			window.location = response.data;
		} else {
			downloadInfo.textContent = response.data;
			downloadInfo.setAttribute('aria-hidden', false);
		}
	}

	function download(e) {
		downloadLoading.setAttribute('aria-hidden', false);
		downloadDataButton.disabled = true;

		var args = {
			'action': wcpd.actions.download,
			'nonce': wcpd.nonce
		};

		wcpdPost(args, downloadResponse, true);
	}

	if (downloadDataButton)
		downloadDataButton.addEventListener('click', download);
})(window, document);

(function (window, document) {
	var deleteAccountButton = document.getElementById('wcpd-delete-account');
	var cancelButton = document.getElementById('deletion-cancel');
	var confirmButton = document.getElementById('deletion-confirm');
	var popin = document.getElementById('deletion-popin');

	function closePopin() {
		document.body.style.overflow = 'scroll';
		document.getElementById('deletion-info').textContent = '';
		popin.setAttribute('aria-hidden', true);
	}

	function openPopin() {
		document.body.style.overflow = 'hidden';
		popin.setAttribute('aria-hidden', false);
	}

	function deleteResponse(response) {
		if (response.success) {
			document.getElementById('deletion-content').setAttribute('aria-hidden', true);
			document.getElementById('deletion-success').setAttribute('aria-hidden', false);

			setTimeout(function () {
				window.location.href = response.data;
			}, 5000);
		} else {
			document.getElementById('deletion-info').textContent = response.data;
		}
	}

	function deleteAccount(e) {
		var args = {
			'action': wcpd.actions.delete,
			'nonce': wcpd.nonce
		};

		wcpdPost(args, deleteResponse, true);
	}

	if (deleteAccountButton)
		deleteAccountButton.addEventListener('click', openPopin);
	if (cancelButton)
		cancelButton.addEventListener('click', closePopin);
	if (confirmButton)
		confirmButton.addEventListener('click', deleteAccount);
})(window, document);