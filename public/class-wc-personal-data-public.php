<?php
if( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'WC_Personal_Data_Public' ) ) :

class WC_Personal_Data_Public {

	private $dir_path;
	private $url_dir_path;
	private $data_endpoint;
	private $data_dir;

	public function __construct() {
		$this->dir_path = plugin_dir_path( __FILE__ );
		$this->url_dir_path = plugins_url( '', __FILE__ ) . '/';
		$this->data_endpoint = 'data';
		$this->data_dir = 'users-data/';

		add_filter( 'woocommerce_account_menu_items', array( $this, 'add_personal_data_link' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'init', array( $this, 'add_personal_data_endpoint' ) );
		add_action( 'woocommerce_account_' . $this->data_endpoint .  '_endpoint', array( $this, 'data_endpoint_content' ) );
		add_action( 'wp_ajax_wcpd_download_data', array( $this, 'download_data' ) );
		add_action( 'wp_ajax_wcpd_delete_account', array( $this, 'delete_account' ) );
	}

	/**
	 * Add personal data link to customer account navigation
	 */
	public function add_personal_data_link() {
		return array(
			'dashboard'          => __( 'Dashboard', 'woocommerce' ),
			'orders'             => __( 'Orders', 'woocommerce' ),
			'downloads'          => __( 'Download', 'woocommerce' ),
			'edit-address'       => __( 'Addresses', 'woocommerce' ),
			'edit-account'       => __( 'Account details', 'woocommerce' ),
			$this->data_endpoint => __( 'Personal data', 'wc-personal-data' ),
			'customer-logout'    => __( 'Logout', 'woocommerce' ),
		);
	}

	/**
	 * Add personal data endpoint to customer account
	 */
	public function add_personal_data_endpoint( $items ) {
		add_rewrite_endpoint( $this->data_endpoint, EP_PAGES );
	}

	/**
	 * Personnal data endpoint content
	 */
	public function data_endpoint_content( $items ) {
		include 'views/personal-data-endpoint.php';
	}

	/**
	 * Get all user data and organize it for CSV creation
	 */
	public function download_data() {
		check_ajax_referer( 'wc-personal-data', 'nonce' );

		$data = array();
		$user = wp_get_current_user();

		if ( 0 == $user->ID )
			wp_send_json_error( __( 'You must be logged to download data.', 'wc-personal-data' ) );

		// Get user information
		$user_meta = get_user_meta( $user->ID );

		foreach ( $user->data as $key => $value )
			$data['user'][] = array( $key, $this->serialize_value( $value ) );

		foreach ( $user_meta as $key => $value )
			$data['user'][] = array( $key, $this->serialize_value( $value ) );

		// Get user comments
		$user_comments = get_comments( array(
			'author_email' => $user->user_email
		) );

		if ( !empty( $user_comments ) ) {
			$comments_headers = array();

			foreach ( $user_comments as $comment ) {
				$comment_values = array();

				foreach ( $comment as $key => $value ) {
					if ( empty( $comments_headers ) || !in_array( $key, $comments_headers ) )
						$comments_headers[] = $key;

					$comment_values[] = $this->serialize_value( $value );
				}

				$data['comments'][] = $comment_values;
			}

			array_unshift( $data['comments'], $comments_headers );
		}

		// Get user orders
		$user_orders = wc_get_orders( array(
			'customer_id' => $user->ID
		) );

		if ( !empty( $user_orders ) ) {
			$orders_headers = array();
			$items_headers = array();

			foreach ( $user_orders as $order ) {
				$order_values = array();
				$order_data = $order->get_data();

				foreach ( $order_data as $key => $value ) {
					if ( empty( $orders_headers ) || !in_array( $key, $orders_headers ) )
						$orders_headers[] = $key;

					$order_values[] = $this->serialize_value( $value );
				}

				$data['orders'][] = $order_values;

				// Get user orders details
				foreach ( $order->get_items() as $item ) {
					$item_values = array();
					$item_data = $item->get_data();

					foreach ( $item_data as $key => $value ) {
						if ( empty( $items_headers ) || !in_array( $key, $items_headers ) )
							$items_headers[] = $key;

						$item_values[] = $this->serialize_value( $value );
					}

					$data['items'][] = $item_values;
				}
			}

			array_unshift( $data['orders'], $orders_headers );
			array_unshift( $data['items'], $items_headers );
		}

		$this->export_data_files( $data, $user->ID );
	}

	private function serialize_value( $value ) {
		return is_array( $value ) ? serialize( $value ) : $value;
	}

	/**
	 * Create a ZIP file with user data, divided in multiple CSV files
	 */
	private function export_data_files( $data, $user_id ) {
		$this->clean_data_dir();

		$created_files = array();
		$site_name = get_bloginfo();
		$zip = new ZipArchive();
		$unique_id = uniqid();
		$zip_name = sanitize_title( $site_name ) . '-data-' . $user_id . '-' . $unique_id . '.zip';
		$zip_path = $this->dir_path . $this->data_dir . $zip_name;

		if ( $zip->open( $zip_path, ZipArchive::CREATE ) !== TRUE )
			wp_send_json_error( __( 'Zip can not be created', 'wc-personal-data' ) );

		foreach ( $data as $label => $content ) {
			$csv_name = $label . '-' . $unique_id . '.csv';
			$csv_path = $this->dir_path . $this->data_dir . $csv_name;
			$csv_file = fopen( $csv_path, 'w' );

			if ( !$csv_file )
				wp_send_json_error( __( 'CSV file can not be created', 'wc-personal-data' ) );

			$created_files[] = $csv_path;

			foreach ( $content as $line ) {
				$csv_line = fputcsv( $csv_file, $line, ';' );

				if ( !$csv_line )
					wp_send_json_error( __( 'Error in CSV line', 'wc-personal-data' ) );
			}

			fclose( $csv_file );
			$zip->addFile( $csv_path, $csv_name );
		}

		$zip->close();

		foreach ( $created_files as $file )
			unlink( $file );

		wp_send_json_success(  $this->url_dir_path . $this->data_dir . $zip_name );
	}

	/**
	 * Delete all ZIP files from users' data directory
	 */
	private function clean_data_dir() {
		$zips = glob( $this->dir_path . $this->data_dir . '*.zip' );

		foreach ( $zips as $zip )
			unlink( $zip );
	}

	/**
	 * Delete user account
	 */
	public function delete_account() {
		check_ajax_referer( 'wc-personal-data', 'nonce' );

		$user = wp_get_current_user();

		if ( 0 == $user->ID )
			wp_send_json_error( __( 'You must be logged to delete your account.', 'wc-personal-data' ) );

		if ( current_user_can( 'manage_options' ) )
			wp_send_json_error( __( 'You have administrator rights, you can not delete your account', 'wc-personal-data' ) );

		if ( wp_delete_user( $user->ID ) )
			wp_send_json_success( get_home_url() );

		wp_send_json_error( __( 'A problem has occured, please retry or contact us to manually delete your account.', 'wc-personal-data' ) );
	}

	public function scripts() {
		global $wp;
		$current_url = home_url( $wp->request ) . '/';

		if ( $current_url !== wc_get_endpoint_url('data') ) return;

		wp_enqueue_style( 'wc-personal-data', plugins_url( 'css/wc-personal-data.css', __FILE__ ), array(), WC_Personal_Data::$plugin_version );
		wp_enqueue_script( 'wc-personal-data', plugins_url( 'js/wc-personal-data.js', __FILE__ ), array(), WC_Personal_Data::$plugin_version, true );
		wp_localize_script( 'wc-personal-data', 'wcpd',
			array(
				'ajaxUrl' => admin_url( 'admin-ajax.php' ),
				'nonce'   => wp_create_nonce( 'wc-personal-data' ),
				'actions' => array( 
					'download' => 'wcpd_download_data',
					'delete'   => 'wcpd_delete_account'
				)
			)
		);
	}
}

new WC_Personal_Data_Public();

endif;