<?php
if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'WC_Personal_Data_Consents' ) ) :

class WC_Personal_Data_Consents {

	public static $consent_duration;

	public function __construct() {
		add_action( 'init', array( $this, 'hourly_check_cron' ) );
		add_action( 'consents_check_cron', array( $this, 'check_consents' ) );
	}

	public function hourly_check_cron() {
		 if ( !wp_next_scheduled ( 'consents_check_cron' ) )
			wp_schedule_event( time(), 'hourly', 'consents_check_cron' );
	}

	public static function create_consent_table() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		$sql = "
			CREATE TABLE IF NOT EXISTS $table_name (
				ID INT NOT NULL AUTO_INCREMENT,
				user_id INT NOT NULL,
				consent_type VARCHAR(255) NOT NULL,
				consent_creation DATETIME NOT NULL,
				consent_expiration DATETIME NOT NULL,
				PRIMARY KEY  (ID)
			);
		";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

	public static function drop_consent_table() {
		wp_clear_scheduled_hook( 'consents_check_cron' );

		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		$sql = "DROP TABLE IF EXISTS $table_name";

		$wpdb->query( $sql );
	}

	public static function add_consent( $user_id, $consent_type ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		return $wpdb->insert( $table_name,
			array(
				'user_id'            => $user_id,
				'consent_type'       => $consent_type,
				'consent_creation'   => date( 'Y-m-d H:i:s' ),
				'consent_expiration' => date( 'Y-m-d H:i:s', strtotime( 'now + 2 years' ) )
			),
			array( '%d', '%s', '%s', '%s' )
		);
	}

	public static function renew_consent( $user_id, $consent_type ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		return $wpdb->update( $table_name,
			array(
				'consent_expiration' => date( 'Y-m-d H:i:s', strtotime( 'now + 2 years' ) )
			),
			array(
				'user_id'            => $user_id,
				'consent_type'       => $consent_type
			)
		);
	}

	public static function remove_consent( $user_id, $consent_type ) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		return $wpdb->delete( $table_name,
			array(
				'user_id'            => $user_id,
				'consent_type'       => $consent_type
			)
		);
	}

	public function check_consents() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'consents';

		$soon_expired_consents = $wpdb->get_results(
			"
				SELECT user_id, consent_type
				FROM $table_name
				WHERE consent_expiration < adddate(NOW(), interval 7 day)
			" 
		);

		if ( !is_null( $soon_expired_consents ) ) {
			foreach ( $soon_expired_consents as $consent ) {
				// send_email
			}
		}

		$expired_consents = $wpdb->get_results(
			"
				SELECT user_id, consent_type
				FROM $table_name
				WHERE consent_expiration < NOW()
			" 
		);

		if ( !is_null( $expired_consents ) ) {
			foreach ( $expired_consents as $consent )
				self::remove_consent( $consent->user_id , $consent->consent_type );
		}
	}
}

new WC_Personal_Data_Consents();

endif;