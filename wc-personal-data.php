<?php
/*
Plugin Name: WC Personal Data
Description: Add features for user's personal data protection compliance
Version: 1.0.0
Author: El Tigre - Software Solutions
Author URI: https://el-tigre.net
*/

if ( !defined( 'ABSPATH' ) ) die;

if ( !class_exists( 'WC_Personal_Data' ) ) :

class WC_Personal_Data {

	public static $plugin_version;

	public function __construct() {
		self::$plugin_version = '1.0.0';

		//include_once 'inc/class-wc-personal-data-consents.php';
		include_once 'public/class-wc-personal-data-public.php';

		add_action( 'plugins_loaded', array( $this, 'textdomain' ) );
		//register_activation_hook( __FILE__, array( 'WC_Personal_Data_Consents', 'create_consent_table' ) );
		//register_deactivation_hook( __FILE__, array( 'WC_Personal_Data_Consents', 'drop_consent_table' ) );
		//register_uninstall_hook( __FILE__, array( 'WC_Personal_Data_Consents', 'drop_consent_table' ) );

		if ( !empty( wp_get_schedule( 'consents_check_cron' ) ) )
			wp_clear_scheduled_hook( 'consents_check_cron' );
	}

	public function textdomain() {
		load_plugin_textdomain( 'wc-personal-data', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}
}

add_action( 'woocommerce_loaded', function() {
	new WC_Personal_Data();
} );

endif;