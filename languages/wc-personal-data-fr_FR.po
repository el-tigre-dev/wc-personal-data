msgid ""
msgstr ""
"Project-Id-Version: WC Personal Data\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-25 10:01+0000\n"
"PO-Revision-Date: 2018-05-25 11:36+0000\n"
"Last-Translator: clement <clement@el-tigre.net>\n"
"Language-Team: Français\n"
"Language: fr_FR\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: public/class-wc-personal-data-public.php:38
msgid "Personal data"
msgstr "Données personnelles"

#: public/class-wc-personal-data-public.php:67
msgid "You must be logged to download data."
msgstr "Vous devez être connecté pour télécharger vos données."

#: public/class-wc-personal-data-public.php:165
msgid "Zip can not be created"
msgstr "Le fichier Zip ne peut être créé"

#: public/class-wc-personal-data-public.php:173
msgid "CSV file can not be created"
msgstr "Le fichier CSV ne peut être créé"

#: public/class-wc-personal-data-public.php:181
msgid "Error in CSV line"
msgstr "Erreur dans une ligne CSV"

#: public/class-wc-personal-data-public.php:215
msgid "You must be logged to delete your account."
msgstr "Vous devez être connecté pour supprimer votre compte."

#: public/class-wc-personal-data-public.php:218
msgid "You have administrator rights, you can not delete your account"
msgstr ""
"Vous avez des droits administrateur, vous ne pouvez pas supprimer votre "
"compte"

#: public/class-wc-personal-data-public.php:223
msgid ""
"A problem has occured, please retry or contact us to manually delete your "
"account."
msgstr ""
"Un problème est survenu, merci de réessayer ou de nous contacter pour "
"supprimer manuellement votre compte."

#: public/views/personal-data-endpoint.php:2
msgid "Download your data"
msgstr "Télécharger vos données"

#: public/views/personal-data-endpoint.php:4
msgid "Download all data we have for your account."
msgstr "Télécharger toutes les données que nous avons pour votre compte."

#: public/views/personal-data-endpoint.php:6
msgid "Download"
msgstr "Télécharger"

#: public/views/personal-data-endpoint.php:10
msgid "We are preparing your data, please wait..."
msgstr "Nous préparons vos données, merci de patienter..."

#: public/views/personal-data-endpoint.php:17
msgid "Delete your account"
msgstr "Supprimer votre compte"

#: public/views/personal-data-endpoint.php:19
msgid "Delete your account and all its data."
msgstr "Supprimer votre compte et toutes ses données."

#: public/views/personal-data-endpoint.php:23
msgid "Account deletion"
msgstr "Suppression de compte"

#: public/views/personal-data-endpoint.php:24
msgid "Do you really want to delete your account and all its data ?"
msgstr "Voulez-vous vraiment supprimer votre compte et toutes ses données ?"

#: public/views/personal-data-endpoint.php:27
msgid "Cancel"
msgstr "Annuler"

#: public/views/personal-data-endpoint.php:28
#: public/views/personal-data-endpoint.php:39
msgid "Delete"
msgstr "Supprimer"

#: public/views/personal-data-endpoint.php:35
msgid ""
"Your account has successfully been deleted. You will be redirected to our "
"homepage."
msgstr ""
"Votre compte a été supprimé avec succès. Vous allez être redirigé vers notre "
"page d'accueil."

#. Name of the plugin
msgid "WC Personal Data"
msgstr "WC Personal Data"

#. Description of the plugin
msgid "Add features for user's personal data protection compliance"
msgstr "Ajoute des fonctionnalités pour la protection des données personnelles"

#. Author of the plugin
msgid "El Tigre - Software Solutions"
msgstr "El Tigre - Software Solutions"

#. Author URI of the plugin
msgid "https://el-tigre.net"
msgstr "https://el-tigre.net"
