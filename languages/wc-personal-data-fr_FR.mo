��          �       �      �  R   �     �  ;        =     Y     `     g  %   {  <   �     �  +   �          &     D     V     d  *   u  >   �  *   �  $   
  S   /     �     �  �  �  l   ?     �  H   �  #        /  	   7     A  .   X  D   �     �  C   �          9     W     q     �  3   �  N   �  7   	  ;   S	  b   �	  #   �	     
   A problem has occured, please retry or contact us to manually delete your account. Account deletion Add features for user's personal data protection compliance CSV file can not be created Cancel Delete Delete your account Delete your account and all its data. Do you really want to delete your account and all its data ? Download Download all data we have for your account. Download your data El Tigre - Software Solutions Error in CSV line Personal data WC Personal Data We are preparing your data, please wait... You have administrator rights, you can not delete your account You must be logged to delete your account. You must be logged to download data. Your account has successfully been deleted. You will be redirected to our homepage. Zip can not be created https://el-tigre.net Project-Id-Version: WC Personal Data
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-25 10:01+0000
PO-Revision-Date: 2018-05-25 11:36+0000
Last-Translator: clement <clement@el-tigre.net>
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ Un problème est survenu, merci de réessayer ou de nous contacter pour supprimer manuellement votre compte. Suppression de compte Ajoute des fonctionnalités pour la protection des données personnelles Le fichier CSV ne peut être créé Annuler Supprimer Supprimer votre compte Supprimer votre compte et toutes ses données. Voulez-vous vraiment supprimer votre compte et toutes ses données ? Télécharger Télécharger toutes les données que nous avons pour votre compte. Télécharger vos données El Tigre - Software Solutions Erreur dans une ligne CSV Données personnelles WC Personal Data Nous préparons vos données, merci de patienter... Vous avez des droits administrateur, vous ne pouvez pas supprimer votre compte Vous devez être connecté pour supprimer votre compte. Vous devez être connecté pour télécharger vos données. Votre compte a été supprimé avec succès. Vous allez être redirigé vers notre page d'accueil. Le fichier Zip ne peut être créé https://el-tigre.net 